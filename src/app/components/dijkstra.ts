import { Vertex } from './vertex';
import { Edge } from './edge';

import { debug } from './config';

export class Dijkstra {
  private unvisited: Array<Vertex>;
  private start: Vertex;
  private end: Vertex;
  private isRuninng: boolean;
  private isFound: boolean;

  constructor(
    private vertices: Array<Vertex>,
    private timeout: number,
    private handleStyle: Function,
    private handleDisplayReset: Function,
    private handleDistances: Function
  ) {
    this.unvisited = this.vertices.slice();
    this.isRuninng = false;
    this.isFound = false;
    this.start = null;
    this.end = null;
  }

  private initVertex(vertex: Vertex) {
    vertex.distance = Infinity;
    vertex.previous = null;
    vertex.visited = false;
    return vertex;
  }

  private quickSort(a: Vertex, b: Vertex) {
    if (a == null && b == null)  { return 0; } else
    if (a == null && b != null)  { return 1; } else
    if (b == null && a != null)  { return -1; } else
    if (a.distance > b.distance) { return -1; } else
    if (a.distance < b.distance) { return 1; } else { return 0; }
  }

  private getVertexSelector(vertex: Vertex): string {
    return vertex != null && `#vx${vertex.id}`;
  }

  private iterate(last: Vertex) {
    // debugger;
    let current: Vertex = this.unvisited.pop();
    let currentSelector: string = this.getVertexSelector(current);

    if (last != null) {
      let selector = this.getVertexSelector(last);
      this.handleStyle(selector, 'visited', true);
      this.handleStyle(selector, 'processing', false);
    }

    if (current == this.end) {
      this.isFound = true;
    } else if (current != null && current.distance !== Infinity) {

      debug && console.log('Processing', current.toString());

      this.handleStyle(currentSelector, 'processing', true);

      for (let i = 0; i < current.edges.length; i++) {
        if (current.edges[i] != null) {
          if( current.distance + current.edges[i].weight < current.edges[i].getVertex(current).distance ) {
            current.edges[i].getVertex(current).distance = current.distance + current.edges[i].weight;
            current.edges[i].getVertex(current).previous = current;
          }
        }
      }

      current.edges = current.edges.map((edge: Edge) => {
        if (edge != null) {
          let distance: number = current.distance + edge.weight;
          let vertex: Vertex = edge.getVertex(current);
          if (distance < vertex.distance) {
            vertex.distance = distance;
            vertex.previous = current;
            return vertex;
          }
        } else {
          return null;
        }
      });

      current.visited = true;
    }

    this.unvisited.sort(this.quickSort);

    if (this.isFound) {
      if (this.end.distance !== Infinity) {
        console.log('Path found. Distance', this.end.distance);

        let pathBack = '';
        current = this.end;

        while(current != this.start) {
          this.handleStyle(currentSelector, 'path', true);
          this.handleStyle(current.findEdge(current.previous), 'path', true);

          pathBack = `[id::${current.id}]: ${current.findEdge(current.previous).weight} > ${pathBack}`;
        }

        pathBack = `[id::${current.id}] > ${pathBack}`;
        this.handleStyle(currentSelector, 'path', true);

        debug && console.log(pathBack);
      } else {
        console.warn('No path found, distance = infinity');
      }

      this.reset();
    } else {
      if (this.unvisited.length > 0) {
        window.setTimeout(
          this.iterate.bind(this),
          this.timeout,
          current);
      } else {
        console.warn('No path found. Path doesn\'t exist');
        this.reset();
      }
    }
  }

  private evaluate(start: Vertex) {
    debug && console.log('Starting search');

    this.handleDisplayReset();

    this.isRuninng = true;
    this.isFound = false;

    this.vertices = this.vertices.map(this.initVertex);
    this.unvisited = this.unvisited.sort(this.quickSort);

    start.distance = 0;

    window.setTimeout(
      this.iterate.bind(this),
      this.timeout,
      null);
  }

  public reset(resetDistances?: boolean) {
    this.isRuninng = false;
    this.start = null;
    this.end = null;

    this.vertices = this.vertices.map((vertex: Vertex) => {
      vertex.isBeginning = false;
      vertex.isEnd       = false;
      vertex.selected    = false;
      vertex.visited     = false;
      vertex.previous    = null;

      if (resetDistances) {
        vertex.distance = 0;
      }

      return vertex;
    });
  }

  /**
  * @description runs Dijkstra algorithm
  * @param {string} elementId - starting point/vertex
  */
  public startFrom(elementId: string) {
    if (!this.isRuninng) {
      let index: string = elementId.substring(2);
      let currentVertex: Vertex = this.vertices[index];
      const selector: string = `#${elementId}`;

      if (!currentVertex.selected) {
        currentVertex.selected = true;

        if (this.start == null) {
          this.reset();
          this.start = currentVertex;
          this.start.isBeginning = true;
          this.handleStyle(selector, 'start', true);

          debug && console.log(`${selector} setted as start`);
        } else if (this.end == null) {
          this.end = currentVertex;
          this.end.isEnd = true;
          this.handleStyle(selector, 'end', true);

          debug && console.log(`${selector} setted as end`);
        } else {
          if (currentVertex.isBeginning) {
            currentVertex.isBeginning = false;
            this.start = null;
            this.handleStyle(selector, 'start', false);
          } else if (currentVertex.isEnd) {
            currentVertex.isEnd = false;
            this.end = null;
            this.handleStyle(selector, 'end', false);
          }
        }

        this.start != null && this.end != null &&
          this.evaluate(this.start);
      }

      this.vertices[index] = currentVertex;

    } else {
      alert('Program already running');
    }
  }
}
