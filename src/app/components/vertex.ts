import { Edge } from './edge';

export interface Vertex {
  id: number;
  x: number;
  y: number;
  distance: number;
  edges: Array<Edge>;
  currentEdge: number;
  selected: boolean;
  isBeginning: boolean;
  isEnd: boolean;
  visited: boolean;
  previous: Vertex;
}

export class Vertex implements Vertex {

  constructor(nodeId: number) {
    this.id = nodeId;
    this.x = 0;
    this.y = 0;
    this.distance = 0;
    this.edges = new Array();
    this.currentEdge = 0;
    this.selected = false,
    this.isBeginning = false,
    this.isEnd = false,
    this.visited = false;
    this.previous = null;
  }

  toString(): string {
    return `Vertex ${this.id}. Current distance: ${this.distance}`;
  }

  addEdge(edge: Edge): void {
    if (this.currentEdge < 4) {
      this.edges[this.currentEdge++] = edge;
    }
  }

  isConnected(node): boolean {
    // return this.edges.some(edge =>
    //   edge != null && (edge.destination.id == node.id || edge.origin.id == node.id));
    for (let i = 0; i < this.edges.length; i++) {
      if (
        (this.edges[i].destination != null && this.edges[i].origin != null)
        &&
        (this.edges[i].destination.id == node.id || this.edges[i].origin.id == node.id)
      ) {
        return true;
      }
      return false;
    }
  }

  connect(vertex: Vertex) {
    if (!this.isConnected(vertex) || !vertex.isConnected(this)) {
      let id: string = `edge ${this.id} ${vertex.id}`;
      let edge: Edge = new Edge(id, this, vertex);
      this.addEdge(edge);
      vertex.addEdge(edge);
      return edge;
    }
    return null;
  }

  findEdge(dest: Vertex) {
    // return this.edges.find(edge =>
    //   edge != null &&
    //     (edge.destination.id == dest.id || edge.origin.id == dest.id));
    for(let i = 0; i < this.edges.length; i++) {
      if(this.edges[i] != null &&
        (this.edges[i].destination.id == dest.id || this.edges[i].origin.id == dest.id)) {
        return this.edges[i];
      }
    }
    return null;
  }
}
