import { Vertex } from './vertex';

export interface Edge {
  id: string;
  weight: number;
  origin: Vertex;
  destination: Vertex;
}

export class Edge {
  constructor(
    id: string,
    origin: Vertex,
    destination: Vertex,
    weight: number = Math.ceil(Math.random() * 10)
  ) {
    this.id = id;
    this.origin = origin || null;
    this.destination = destination || null;
  }

  getVertex(vertex: Vertex): Vertex {
    return this.origin != null && this.origin.id == vertex.id ?
      this.destination : this.origin;
  }

  toString(): string {
    return `Edge connecting ${this.origin.id} to ${this.destination.id} with weight ${this.weight}`;
  }
}
