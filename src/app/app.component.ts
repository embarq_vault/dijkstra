import { Component, OnInit } from '@angular/core';

import * as d3 from 'd3';

import { Vertex } from './components/vertex';
import { GraphMap } from './components/graph';
import { Dijkstra } from './components/dijkstra';

@Component({
  selector: 'app-root',
  template: '<svg class="app-canvas" id="app-canvas"></svg>',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private graphMap: GraphMap;
  private dijkstraAlgorithm: Dijkstra;

  private start: Vertex;
  private end: Vertex;

  constructor() { }

  ngOnInit() {
    this.start = null;
    this.end = null;

    let interval = 50;

    this.graphMap = new GraphMap();
    this.dijkstraAlgorithm = new Dijkstra(
      this.graphMap.vertexArray,
      interval,
      this.graphMap.styleElement,
      this.graphMap.redraw,
      this.graphMap.getDistances);


    window.onload = () => {
      this.graphMap.init();
      Array.from(document.querySelectorAll('.vertex'))
        .map(element =>
          element.addEventListener('click', this.handleClick.bind(this)));
    }
  }

  private handleClick(event) {
    this.dijkstraAlgorithm.startFrom(event.target.id);
  }
}
