import * as d3 from 'd3';

import { Vertex } from './vertex';
import { Edge } from './edge';

export class GraphMap {
  private vertexCount    : number;
  private edgeCount      : number;
  private vertices       : Array<Vertex>;
  private edges          : Array<Edge>;

  private map;
  private scale;
  private radius;
  private canvas;
  private lines;
  private weights;
  private circle;
  private distances;
  private bardata;
  private barscale;
  private visitedBar;
  private visitedBarText;

  private interval       : number;
  private randomFrequency: number;
  private max: {
    width: number;
    height: number;
  };

  constructor(private placeholder: string = '.app-canvas') {
    this.placeholder = placeholder;
    this.vertices = new Array();
    this.edges = new Array();
    this.vertexCount = 0;
    this.edgeCount = 0;
    this.randomFrequency = 0.75;
    this.max = {
      width: 2,
      height: 2
    };
    this.map = {
      width: 640,
      height: 640
    }

    this.generatePoints();
  }

  get vertexArray(): Array<Vertex> {
    return this.vertices;
  }

  private generatePoints(): void {
    for(let i: number = 0; i < this.max.width; i++) {
      for(let j: number = 0; j < this.max.height; j++) {
        var vertex = new Vertex(this.vertexCount++);
        vertex.x = (i * 30) + 15;
        vertex.y = (j * 30) + 15;
        this.vertices[(i * this.max.height) + j] = vertex;
      }
    }

    for(let i: number = 0; i < this.max.width; i++) {
      for(let j: number = 0; j < this.max.height; j++) {
        let offset = (i * this.max.height) + j;
        let vertex = this.vertices[offset];

        if (
          j < this.max.height - 1
          &&
          Math.random() < this.randomFrequency
        ) {
          this.edges[this.edgeCount++] =
            vertex.connect(this.vertices[offset + 1]);
        }

        if (
          i < this.max.width - 1
          &&
          Math.random() < this.randomFrequency
        ) {
          this.edges[this.edgeCount++] =
            vertex.connect(this.vertices[offset + this.max.height]);
        }
      }
    }
  }

  public redraw() {
    this.lines && this.lines.attr('class', 'line');
    if (this.circle) {
      this.circle.attr('class', d => {
        let className = 'vertex';
        className += d.selected ? 'selected' : '';
        className += d.isBeginning ? 'start' : '';
        className += d.isEnd ? 'end' : '';
        return className;
      });
    }

    if (this.distances) {
      this.distances.text(({ distance }) =>
        distance != Infinity && distance > 0 ? distance : '');
    }
  }

  public init() {
    this.scale = {
      x: d3.scaleLinear()
        .domain([0, this.max.width * 30])
        .range([0, this.map.width]),
      y: d3
        .scaleLinear()
        .domain([0, this.max.height * 30])
        .range([0, this.map.height - 50])
    };

    this.radius = this.scale.x(7);

    this.canvas = d3
      .select(this.placeholder)
      .attr('width', this.map.width)
      .attr('height', this.map.height);

    this.lines = d3
      .select(this.placeholder)
      .selectAll('line')
      .data(this.edges)
      .enter()
      .append('line')
      .attr('x1', d => this.scale.x(d.origin.x))
      .attr('y1', d => this.scale.y(d.origin.y))
      .attr('x2', d => this.scale.x(d.destination.x))
      .attr('y2', d => this.scale.y(d.destination.y))
      .attr('class', 'line')
      .attr('id', d => d.id);

    this.weights = d3
      .select(this.placeholder)
      .selectAll('.weight')
      .data(this.edges)
      .enter()
      .append('text')
      .attr('x', d =>
        d.origin.x == d.destination.x ?
          this.scale.x(d.origin.x) :
          this.scale.y((d.origin.x + d.destination.x) / 2))
      .text(d => d.weight)
      .attr('class', 'weight');

    this.circle = d3
      .select(this.placeholder)
      .selectAll('circle')
      .data(this.vertices)
      .enter()
      .append('circle')
      .attr('r', this.radius)
      .attr('class', d => d.selected ?
        'vertex selected' :
        'vertex')
      .attr('cx', d => this.scale.x(d.x))
      .attr('cy', d => this.scale.y(d.y))
      .attr('id', d => `vx${d.id}`);

    this.distances = d3
      .select(this.placeholder)
      .selectAll('.distance')
      .data(this.vertices)
      .enter()
      .append('text')
      .attr('x', d => this.scale.x(d.x - 2))
      .attr('y', d => this.scale.y(d.y + 2))
      .text(d =>
        d.distance != Infinity && d.distance > 0 ? d.distance : '')
      .attr('class', 'distance');
  }

  public highlightVertex(
    vertex: Vertex,
    className: string,
    flag: boolean
  ) {
    if (vertex != null) {
      let { id } = vertex;
      let selection = d3.select(`#vx${id}`);
      return selection != null &&
        selection.classed(className, flag);
    }
  }

  public highlightEdge(
    edge: Edge,
    className: string,
    flag: boolean
  ) {
    if (edge != null) {
      let { id } = edge;
      let selection = d3.select(`#${id}`);
      return selection != null &&
        selection.classed(className, flag);
    }
  }

  public styleElement(
    selector: string,
    className: string,
    flag: boolean
  ) {
    let selection = d3.select(selector);
    return selection != null &&
      selection.classed(className, flag);
  }

  public getDistances() {
    return this.distances;
  }
}
